apply plugin: 'com.android.application'
// keep some basic stuff from the android.jar platform code
// see https://github.com/bjoernQ/unmock-plugin#how-to-use
apply plugin: 'de.mobilej.unmock'

/* gets the version name from the latest Git tag */
def getVersionName = { ->
    def stdout = new ByteArrayOutputStream()
    exec {
        commandLine 'git', 'describe', '--tags', '--always'
        standardOutput = stdout
    }
    return stdout.toString().trim().substring(1)
}

repositories {
    mavenCentral()
    google()
    maven { url "https://jitpack.io" }
}

dependencies {
    // androidX stuff
    implementation 'androidx.appcompat:appcompat:1.7.0-alpha01'
    implementation 'androidx.annotation:annotation:1.5.0'
    implementation 'androidx.preference:preference:1.2.0'
    implementation 'androidx.recyclerview:recyclerview:1.2.1'
    implementation 'androidx.localbroadcastmanager:localbroadcastmanager:1.1.0'
    implementation 'com.google.android.material:material:1.7.0'
    implementation 'androidx.constraintlayout:constraintlayout:2.1.4'
    implementation 'androidx.swiperefreshlayout:swiperefreshlayout:1.1.0'
    // general support library
    implementation 'com.google.guava:guava:31.1-android'
    // image loading
    implementation 'com.nostra13.universalimageloader:universal-image-loader:1.9.5'
    // FAB-menu/speed-dial
    implementation "com.leinardi.android:speed-dial:3.3.0"
    // QR
    implementation('com.journeyapps:zxing-android-embedded:4.3.0')
    // Tor proxy support
    implementation 'info.guardianproject.netcipher:netcipher:2.1.0'
    // crash reports
    implementation 'com.github.ligi.tracedroid:lib:d0048245ab'
    implementation 'com.github.ligi.tracedroid:supportemail:d0048245ab'
    // index-json parsing
    def jackson_version = '2.13.4'
    implementation "com.fasterxml.jackson.core:jackson-core:$jackson_version"
    implementation "com.fasterxml.jackson.core:jackson-annotations:$jackson_version"
    implementation "com.fasterxml.jackson.core:jackson-databind:$jackson_version"

    // Android Studio continues to suggest upgrading that to 2.0.x
    // but that needs newer AGP which needs a beta of Android Studio...
    //noinspection GradleDependency
    coreLibraryDesugaring 'com.android.tools:desugar_jdk_libs:1.2.2'

    // Don't update this to junit5 or remove this, it makes Roboelectric tests fail with SIGSEGV (what?)
    // https://git.bubu1.eu/Bubu/fdroidclassic/-/jobs/666
    testImplementation 'junit:junit:4.13.2'
    testImplementation 'org.mockito:mockito-core:4.8.0'
    testImplementation 'com.google.truth:truth:1.1.3'
    testImplementation 'androidx.test:core:1.5.0'
    testImplementation 'org.robolectric:robolectric:4.8.2'
}

def myApplicationId = "eu.bubu1.fdroidclassic"
def privilegedExtensionApplicationId = '"eu.bubu1.privext"'
def privilegedExtensionApplicationIdFallback1 = '"org.fdroid.fdroid.privileged.do"'
def privilegedExtensionApplicationIdFallback2 = '"org.fdroid.fdroid.privileged.root"'


android {
    compileSdkVersion 33

    compileOptions {
        sourceCompatibility JavaVersion.VERSION_11
        targetCompatibility JavaVersion.VERSION_11
        encoding = "UTF-8"
        coreLibraryDesugaringEnabled true
    }

    buildTypes {
        release {
            minifyEnabled true
            shrinkResources true
            proguardFiles getDefaultProguardFile(
                    'proguard-android-optimize.txt'),
                    'proguard-rules.pro'
        }
        all {
            buildConfigField "String", "PRIVILEGED_EXTENSION_PACKAGE_NAME", privilegedExtensionApplicationId
            buildConfigField "String", "PRIVILEGED_EXTENSION_PACKAGE_NAME_FALLBACK1", privilegedExtensionApplicationIdFallback1
            buildConfigField "String", "PRIVILEGED_EXTENSION_PACKAGE_NAME_FALLBACK2", privilegedExtensionApplicationIdFallback2
        }
        debug {
            applicationIdSuffix ".debug"
            resValue "string", "applicationId", myApplicationId + applicationIdSuffix
            versionNameSuffix "-debug"
            println 'buildTypes.debug defaultConfig.versionCode ' + defaultConfig.versionCode
        }
    }

    testOptions {
        unitTests {
            includeAndroidResources = true
        }
    }


    defaultConfig {
        minSdkVersion 22
        //noinspection ExpiredTargetSdkVersion
        targetSdkVersion 25
        versionCode 1301
        applicationId myApplicationId
        resValue "string", "applicationId", myApplicationId
        versionName getVersionName()
        resConfigs "en", "ar", "ast", "bg", "ca", "cs", "da", "de", "el", "eo", "es", "et",
                "eu", "fa", "fi", "fr", "gl", "he", "hi", "hr", "hu", "id", "is", "it",
                "ja", "ko", "lt", "lv", "mk", "my", "nb", "nl", "pl", "pt-rBR", "pt-rPT", "ro",
                "ru", "sc", "si", "sk", "sl", "sn", "sq", "sr", "sv", "ta", "th", "tr", "ug",
                "uk", "ur", "vi", "zh-rCN", "zh-rHK", "zh-rTW"
    }
    packagingOptions {
        resources {
            excludes += ['META-INF/LICENSE', 'META-INF/LICENSE.txt', 'META-INF/NOTICE', 'META-INF/NOTICE.txt', 'META-INF/INDEX.LIST', '.readme']
        }
    }


    lint {
        abortOnError true
        checkReleaseBuilds false
        disable 'MissingTranslation'
        error 'AppCompatMethod', 'NestedScrolling', 'StringFormatCount', 'UnsafeProtectedBroadcastReceiver'
        htmlReport true
        textReport false
        xmlReport false
    }
    namespace 'org.fdroid.fdroid'
}
